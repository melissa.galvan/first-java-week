package exo.simplon.promo16.bases;

public class Variables {
    public void first() {
        int myAge = 27;
        String promoName = "Promo16";
        boolean loveJava = true;
        String[] languages = {
                "HTML", "CSS", "JavaScript", "PHP", "SQL", "JAVA"
        };

        for (String language : languages) {
            System.out.println(language);
        }
        System.out.println(myAge + " ans | " + promoName + " | " + loveJava);
    }

    public void withParameters(String str, int nmbr) {
        System.out.println(str + nmbr);
    }

    public String withReturn() {
        return "Le return";
    }
}
